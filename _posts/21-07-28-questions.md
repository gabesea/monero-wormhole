---
layout: log
title: Questions
description: Questions and answers related to the Monero Wormhole project.
---

You might have some questions. I will try and address those here.

1. Who are you?

A private citizen. Just like you, probably.

2. Why Monero and not Bitcoin?

Bitcoin can be *dirty*. It is not fungible.

Transparent by default. Optionally private. Not good.

Monero is fungible. Monero is liberation money, true digital cash. 

Private by default. Optionally transparent. Good.

3. Why Monero Wormhole?

To help people focus on building their craft and sharing with other like-minded humans in a safe, private environment.

4. Why email + RSS over social media?

There's very little in terms of real *social* connection going on with all that.

Mostly noise.

A plain old email address should suffice. Just contact real humans directly.

That's real peer to peer.

And if you wish to stay in the loop, that's where that ancient RSS comes into play.

All you need, if you really think about it.

5. How can I get involved?

Just connect to the community. 

Fork this website, make it your own and share your craft.

Then email us, we are all here waiting for new arrivals.

Already have a site? Just put up a wormhole page and link to some people you like in the community.

Again, don't be shy and reach out to the people you put up on your page.

Use your Monero to hire people or tip someone just to put up a smile on their face today!

6. Is this free?

Yes, if you're referring to the code. It's free and open source code.

The only thing that costs money is the purchase of your own domain name.

If you have literally **zero** budget you can still use a Netlify free subdomain: *yourname.netlify.app*

Hosting is free as well, as long as you use Netlify's free plan.

The SSL is free, provided by Let's Encrypt via Netlify.

The Netlify CMS that provides the user friendly backend is also free and open source.

You will also need a Gitlab account, which is free.

And by the way, use the logo as well if you have none to begin with. The [graphic artist](/#) created it pro bono and is part of the community, naturally.

I think that's all.

[Get started](/getting-started).


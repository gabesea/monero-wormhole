---
layout: log
title: Hello world, what's this?
description: Monero Wormhole introduction.
---

## Monero Wormhole

This template is for each and every crafter out there: 

*artists, coders, technicians, musicians, writers, engineers, hackers, story tellers, blacksmiths, poets, magicians, gamers*,

and all of you good people out there that want to share your work with the world both pro bono and in exchange for cash, digital cash, **Monero**.

[Use it](/getting-started). Keep on crafting. Connect with others through the wormhole.

<pre>
─────█─▄▀█──█▀▄─█─────
────▐▌──────────▐▌────
────█▌▀▄──▄▄──▄▀▐█────
───▐██──▀▀──▀▀──██▌───
──▄████▄──▐▌──▄████▄──
</pre>

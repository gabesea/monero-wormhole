---
layout: log
title: Getting started
description: Getting started with Monero Wormhole.
---

## Inspect the code

It's publicly available on [Gitlab](/#). Look around.

## Fork it

Grab a copy and make it your own. 

Plug in your own logo, edit the website's name, tweak everything to your liking.

## Start crafting & document your work

Share with the world what your normal crafting day looks like. 

Are you a song writer? Walk us through your process of creating a love song.

Chef? Share some tips and tricks for a healthy and tasty diet.

Ninja? Well, someone out there might want to learn some of those skills.

## Use the wormhole

Discover other humans like you and me, people that actually use Monero for good.

Connect with them via email and let them know that you've linked them to your wormhole page.

Maybe they will do the same.

## Need help?

Don't know how to deploy your website? Contact someone that already has it online, they are quite likely to help for free.

Want a custom logo? Someone in the wormhole will do it, probably for a few coins or maybe pro bono, if you ask really nice.

Got more questions? Well, you already know what to do..

Click on the [Wormhole](/wormhole) page and navigate until you reach someone that can assist. Email. Easy.

You will find the community is rather helpful and nice, provided you are not trying to do something that goes against our [Manifesto](/manifesto.txt) or is negative to others.

Good luck and welcome!

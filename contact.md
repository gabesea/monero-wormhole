---
layout: page
title: Contact
description: Contact, about and tips.
xmr_address: 
xmr_qr: 
---

`Edit *_config.yml* or */admin* and input your own email address here.`

I have deliberately **not** linked an email address here or any other contact details.

That will naturally make you connect with the community directly, in a peer to peer decentralized way.

**Use the [Wormhole](/wormhole) instead**.

No social media. Notifications via [RSS](/sitemap.xml).

## About

To get started just [fork](/#) this project on Gitlab and proceed from there.

If you want to help me improve this, open an issue or a merge request in the project's [repository](/#).

### Tips (XMR)

`Edit *contact.md* or */admin* and input your own XMR address here.`

{% if page.xmr_address != nil %}
Any transfers to this address will be considered contributions to this project.

`{{ page.xmr_address }}`{:class="xmr-address"}
{% endif %}

{% if page.xmr_qr != nil %}
![XMR QR]({{ page.xmr_qr }}){:class="xmr-qr"}
{% endif %}
                                
Thanks!

---
layout: page
title: Wormhole
description: My community contacts.
---

This is just one entry point into the community. Your address book with community contacts.

I have included a few contacts/sites to act as placeholders:

`Edit *_data/humans.yml* or */admin* to make your wormhole point to different humans.`

<ol>
{% for human in site.data.humans.items %}
<li><a href="{{ human.url }}">{{ human.name }}</a></li>
{% endfor %}
</ol>

Want to be linked to this wormhole page?

If you've read the [Manifesto](/manifesto.txt) and it resonates, send a pull request. 

*Note that this page is not meant to be functioning as an index of every community member out there. That's centralization. We want peer to peer. Discover peers by navigating through individual sites and connecting with others.*
